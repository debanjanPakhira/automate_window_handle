package demo;


import java.io.File;
import java.io.IOException;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import io.github.bonigarcia.wdm.WebDriverManager;
//Selenium Imports
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;
///


public class Automate_window_handle {
    ChromeDriver driver;
    public Automate_window_handle()
    {
        System.out.println("Constructor: TestCases");
        WebDriverManager.chromedriver().timeout(30).setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

    }

    public void endTest()
    {
        System.out.println("End Test: TestCases");
        driver.close();
        driver.quit();

    }


    public  void testCase01() throws InterruptedException, IOException {
        System.out.println("Start Test case: testCase01");
        driver.get("https://www.w3schools.com/jsref/tryit.asp?filename=tryjsref_win_open");
        driver.switchTo().frame(1);
        WebElement try_it = driver.findElement(By.xpath("//html/body/button"));
        try_it.click();
        Thread.sleep(2000);
        Set<String> windowHandles = driver.getWindowHandles();
        for(String windowHandle:windowHandles){
            if(!driver.getWindowHandle().equals(windowHandle)){
                driver.switchTo().window(windowHandle);
            }
        }


        System.out.println("The URL of the new tab: " + driver.getCurrentUrl());

        File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        File destFile = new File("screenshot_of_new_tab.png");
        FileUtils.copyFile(screenshot, destFile);

        driver.switchTo().window(windowHandles.iterator().next());
        System.out.println("original window: "+driver.getTitle());

        System.out.println("end Test case: testCase02");
    }


}
